# -*- coding: utf-8 -*-

from distutils.core import setup
from glob import glob
import subprocess


scripts = glob('scripts/*')
command = ['git', 'describe', '--tags']
version = subprocess.check_output(command).decode().strip()

setup(name='label-image-cleanup',
      version=version,
      description='Clean up a label image',
      author='Shuo Han',
      author_email='shan50@jhu.edu',
      install_requires=['numpy', 'scipy'],
      packages=['label_image_cleanup'],
      scripts=scripts)

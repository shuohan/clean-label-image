Label Image Cleanup
===================

Examples
--------

1. Command line:

.. code-block:: bash

   $ cleanup_label_image.py -i input_image.nii.gz -o output_image.nii.gz
   $ cleanup_label_image.py -h # for more information

2. Python library:

.. code-block:: python

   from label_image_cleanup import cleanup
   cleaned = cleanup(image, threshold=0.9)


API
---

.. automodule:: label_image_cleanup
.. autofunction:: cleanup

.. automodule:: label_image_cleanup.funcs
.. autoclass:: Offsets
   :members:
.. autofunction:: majority_vote
.. autofunction:: dilate
.. autofunction:: onehot


Index
=====
* :ref:`genindex`

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import nibabel as nib

from label_image_cleanup.funcs import dilate


obj = nib.load('mask.nii.gz')
mask = obj.get_data() > 0
dilated = dilate(mask)
out = nib.Nifti1Image(dilated, obj.affine, obj.header)
out.to_filename('dilated_mask.nii.gz')


def calc_toy():
    mask = np.zeros((5, 5), dtype=bool)
    mask[1, 1 : 4] = True
    mask[2, 2 : 4] = True
    return mask

mask = calc_toy()
dilated_mask = dilate(mask)
print(mask)
print(dilated_mask)
plt.figure()
plt.imshow(mask)
plt.figure()
plt.imshow(dilated_mask)
plt.show()

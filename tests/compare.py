#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import nibabel as nib
import numpy as np

image1 = nib.load(sys.argv[1]).get_data()
image2 = nib.load(sys.argv[2]).get_data()
print(np.array_equal(image1, image2))

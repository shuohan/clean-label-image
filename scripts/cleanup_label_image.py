#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse 

parser = argparse.ArgumentParser(description='Clean up a label image.')
parser.add_argument('-i', '--image', help='Label image to clean up.',
                    required=True)
parser.add_argument('-o', '--output', help='Output filename.', required=True)
help = 'Keep more original labels if larger. Between 0 and 1.'
parser.add_argument('-t', '--threshold', help=help, default=0.9, type=float)
args = parser.parse_args()


import nibabel as nib
from label_image_cleanup.funcs import cleanup


obj = nib.load(args.image)
label_image = obj.get_data().round().astype(int)
cleaned = cleanup(label_image, threshold=args.threshold)
cleaned_obj = nib.Nifti1Image(cleaned, obj.affine, obj.header)
cleaned_obj.to_filename(args.output)

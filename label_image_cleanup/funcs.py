# -*- coding: utf-8 -*-

import numpy as np
from scipy.ndimage.morphology import binary_fill_holes
from scipy.ndimage.measurements import label as find_connected_components


def cleanup(label_image, threshold=0.9):
    """Cleans up a label image.

    This algorithm has the following steps:

    1. Calcualte connected components (cc) of each labeled region.
    2. Categorize each cc into larger or smaller according to the size of
       the largest cc times the input variable ``threshold``.
    3. Update the label of a smaller cc by majority voting of bordering
       larger cc.
    4. Fill holes. The label of the filled are also determined by majority
       voting of the bordering pixels/voxels.

    Args:
        label_image (numpy.ndarray[int]): The label image to clean.
        threshold (float, optional): Categorize larger and smaller cc.

    Returns:
        numpy.ndarray: The cleaned up label image.

    """
    hole_se = calc_sphere_se(2)
    conn_se = calc_conn_se()
    spatial_dims = tuple(np.arange(len(label_image.shape)) + 1)
    labels = np.unique(label_image)

    # Categorize connected components into larger and smaller
    largers = list()
    smallers = list()
    for label in labels[1:]:
        mask = label_image == label
        cc, num_cc = find_connected_components(mask, structure=conn_se)
        cc = onehot(cc, num_cc)[1:, ...]
        sizes = np.sum(cc, axis=spatial_dims)
        max_ind = np.argmax(sizes)
        max_size = sizes[max_ind]
        larger_inds = sizes > (max_size * threshold)
        smaller_inds = np.logical_not(larger_inds)
        larger = np.sum(cc[larger_inds, ...], axis=0)
        largers.append(larger)
        smallers.extend(cc[smaller_inds, ...])

    # Convert cc into a label image
    background = 1 - np.sum(largers, axis=0)
    largers = np.stack([background, *largers], axis=0)
    cleaned = labels[np.argmax(largers, axis=0)]

    cleaned = majority_vote(smallers, cleaned)

    union = cleaned > 0
    holes = binary_fill_holes(union, structure=hole_se) ^ union
    holes, num_holes = find_connected_components(holes, structure=conn_se)
    holes = onehot(holes, num_holes)[1:, ...]
    cleaned = majority_vote(holes, cleaned)

    return cleaned


def majority_vote(regions, label_image):
    """Updates region labels using majority voting.

    Args:
        regions (list[numpy.ndarray]): The region to change labels.
        label_image (numpy.ndarray): The label image to find bordering labels
            for each region.

    Returns:
        numpy.ndarray: The updated label image with new labels of regions
        written in.

    """
    # The labels at region are 0 so do not need to substract to get neighbors
    for region in regions:
        neighbors = label_image[dilate(region)]
        counts = np.bincount(neighbors[neighbors>0].astype(int))
        if len(counts) > 0:
            label_image[region] = np.argmax(counts)
    return label_image


class Offsets:
    """Helper class for one pixel/voxel binary dilation.

    To dilate a binary image by one pixel/voxel, we only need to set their
    connected pixels/voxels to True. Call :meth:`get` with the image dimension
    to get the index offsets. For a 2D image, the offsets are [-1, 0], [1, 0],
    [0, -1], [0, 1], i.e. 4-connected. For a 3D image, the offsets are
    [-1, 0, 0], [1, 0, 0], [-1, 0, 1], etc., i.e. 6-connected.

    """
    _offsets = dict()

    @classmethod
    def get(cls, dim):
        """Returns the offsets for images with ``dim`` dimensions.

        Args:
            dim (int): Hello
        
        """
        if dim not in cls._offsets:
            cls._offsets[dim] = cls._calc_offsets(dim)
        return cls._offsets[dim]

    @staticmethod
    def _calc_offsets(dim):
        offsets = [np.zeros((dim, 1), dtype=int)]
        for i in range(dim):
            offset = np.zeros((dim, 1), dtype=int)
            offset[i] = -1
            offsets.append(offset)
            offsets.append(-offset)
        return offsets


def dilate(mask):
    """Dilates a binary image for one pixel/voxel.

    It is faster than :func:`scipy.ndimage.morphology.binary_dilation` for large
    images when only one pixel/voxel dilation are needed. 4-connected for a 2D
    image and 6-connected for a 3D image. See :class:`Offsets`.

    Args:
        mask (numpy.ndarray): The binary image to dilate.
    
    Returns:
        numpy.ndarray: The dilated binary image. 

    """
    indices = np.vstack(np.where(mask))
    dim = indices.shape[0]
    offsets = Offsets.get(dim)
    dilated_indices = np.hstack([indices + o for o in offsets])
    dilated = np.zeros_like(mask)
    dilated_indices = tuple(np.vsplit(dilated_indices, dim))
    dilated[dilated_indices] = True
    return dilated


def onehot(label_image, num_labels=None):
    """Performs one-hot encoding.

    Args:
        label_image (numpy.ndarray): The label image to convert to be encoded.
        num_labels (int): The total number of labels. If ``None``, it will be
            calcualted as the number of unique values of input ``label_image``.

    Returns:
        numpy.ndarray: The encoded label image. The first dimension is channel.

    """
    if num_labels is None:
        num_labels = len(np.unique(label_image)) - 1 # without background 0
    result = np.zeros((num_labels + 1, label_image.size), dtype=bool)
    result[label_image.flatten(), np.arange(label_image.size)] = 1
    result = result.reshape(-1, *label_image.shape)
    return result


def calc_sphere_se(radius):
    """Calculates a spherical structuring element with shape [2*radius + 1] * 3.

    Args:
        radius (int): The radius of the structuring element.

    Returns:
        numpy.ndarray: The structuring element.

    """
    grid = np.meshgrid(*([range(2 * radius + 1)] * 3), indexing='ij')
    se = np.add.reduce([(g - radius) ** 2 for g in grid]) <= radius ** 2
    return se


def calc_conn_se():
    """Calculates the structuring element to find cc with 6-connectivity.

    Returns:
        numpy.ndarray: The structuring element.

    """
    se = np.array([[[0, 0, 0], [0, 1, 0], [0, 0, 0]],
                   [[0, 1, 0], [1, 1, 1], [0, 1, 0]],
                   [[0, 0, 0], [0, 1, 0], [0, 0, 0]]], dtype=bool)
    return se
